"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//http://docs.nwjs.io/en/latest/References/Window/#windowgetwindow_object
var nw, win, window, document;
function main(nwjsModule) {
    nw = nwjsModule;
    win = nw.Window.get();
    window = win.window;
    document = window.document;
    var url = "http://localhost:8080/";
    var can = new Canvas(document.getElementById("can"));
    var client = new HttpClient();
    var outputBox = document.getElementById("output");
    var outputButton = document.getElementById("outputbutton");
    var resetButton = document.getElementById("resetbutton");
    outputBox.innerHTML = "";
    outputButton.addEventListener("click", function (e) {
        client.postCon(url, can.charsData, true, function (data) {
            outputBox.innerHTML += "\nfrom server:(" + data + ")\n";
        });
        outputBox.innerHTML = JSON.stringify(can.charsData);
    }, false);
    resetButton.addEventListener("click", function (e) {
        can.clearCanvas();
        outputBox.innerHTML = "";
    }, false);
    return;
}
exports.main = main;
var Canvas = /** @class */ (function () {
    function Canvas(canvasElem) {
        var _this = this;
        this.mousedownf = false;
        this.can = canvasElem;
        this.cc = this.can.getContext("2d");
        this.style = this.can.style;
        this.can.width = 508;
        this.can.height = 508;
        this.style.border = "1px solid";
        this.style.touchAction = "none";
        this.can.addEventListener("mousedown", function (e) { _this.getXYfromMouse(e, 0); }, false);
        this.can.addEventListener("mousemove", function (e) { _this.getXYfromMouse(e, 1); }, false);
        this.can.addEventListener("mouseup", function (e) { _this.getXYfromMouse(e, 2); }, false);
        this.can.addEventListener("mouseout", function (e) { _this.getXYfromMouse(e, 2); }, false);
        this.can.addEventListener("touchstart", function (e) { _this.getXYfromTouch(e, 0); }, false);
        this.can.addEventListener("touchmove", function (e) { _this.getXYfromTouch(e, 1); }, false);
        this.can.addEventListener("touchend", function (e) { _this.getXYfromTouch(e, 2); }, false);
        this.can.addEventListener("touchcancel", function (e) { _this.getXYfromTouch(e, 2); }, false);
        this.chars = new Moji();
    }
    Canvas.prototype.getXYfromTouch = function (e, n) {
        var cancli = this.can.getBoundingClientRect();
        var _a = [
            e.changedTouches.item(0).pageX - cancli.left,
            e.changedTouches.item(0).pageY - cancli.top
        ], x = _a[0], y = _a[1];
        if (n == 0) {
            this.bx = x;
            this.by = y;
            this.chars.start();
        }
        else {
            this.draw(x, y);
        }
        this.chars.update(x, y);
    };
    Canvas.prototype.getXYfromMouse = function (e, n) {
        var cancli = this.can.getBoundingClientRect();
        var _a = [
            e.pageX - cancli.left,
            e.pageY - cancli.top
        ], x = _a[0], y = _a[1];
        if (n == 0) {
            this.mousedownf = true;
            this.bx = x;
            this.by = y;
            this.chars.start();
            this.chars.update(x, y);
        }
        else if (this.mousedownf) {
            this.draw(x, y);
            if (n == 2 || this.isOutDisplay(x, y))
                this.mousedownf = false;
            this.chars.update(x, y);
        }
    };
    Canvas.prototype.draw = function (x, y) {
        this.cc.save();
        this.cc.beginPath();
        this.cc.strokeStyle = "rgb(0,0,0)";
        this.cc.lineWidth = 2;
        this.cc.lineTo(this.bx, this.by);
        this.cc.lineTo(x, y);
        this.cc.stroke();
        this.cc.restore();
        this.bx = x;
        this.by = y;
    };
    Object.defineProperty(Canvas.prototype, "charsData", {
        get: function () {
            return this.chars.line;
        },
        enumerable: true,
        configurable: true
    });
    Canvas.prototype.isOutDisplay = function (x, y) {
        return (x < 0 || this.can.width < x || y < 0 || this.can.height < y) ? true : false;
    };
    Canvas.prototype.clearCanvas = function () {
        this.cc.clearRect(0, 0, this.can.width, this.can.height);
        this.chars.init();
    };
    Canvas.prototype.outputPngData = function () {
        var image = this.can.toDataURL("image/png");
        return image;
    };
    return Canvas;
}());
var Moji = /** @class */ (function () {
    function Moji() {
        this.line = [];
    }
    Moji.prototype.start = function () {
        this.startf = 0;
    };
    Moji.prototype.update = function (x, y) {
        if (this.startf === undefined)
            return;
        this.line.push([this.startf, x, y]);
        if (!this.startf)
            this.startf = 1;
    };
    Moji.prototype.init = function () {
        this.line = [];
    };
    return Moji;
}());
var HttpClient = /** @class */ (function () {
    function HttpClient() {
        this.httpObj = new XMLHttpRequest();
    }
    HttpClient.prototype.postImage = function (url, Base64Data, callback) {
        var _this = this;
        this.httpObj.open("POST", url, true);
        this.httpObj.setRequestHeader("Content-Type", "application/json");
        var blob = atob(Base64Data);
        this.httpObj.send(blob);
        this.httpObj.onreadystatechange = function () {
            if (_this.httpObj.readyState == 4) {
                callback(_this.httpObj.responseText);
            }
        };
        return undefined;
    };
    HttpClient.prototype.postCon = function (url, postData, fasyn, callback) {
        var _this = this;
        if (callback === void 0) { callback = undefined; }
        this.httpObj.open("POST", url, fasyn);
        this.httpObj.setRequestHeader("Content-Type", "application/json");
        this.httpObj.send(JSON.stringify(postData));
        if (fasyn) {
            this.httpObj.onreadystatechange = function () {
                if (_this.httpObj.readyState == 4) {
                    callback(_this.httpObj.responseText);
                }
            };
            return undefined;
        }
        else {
            callback = callback || (function (data) { return data; });
            if (this.httpObj.readyState == 4) {
                return callback(this.httpObj.responseText);
            }
        }
    };
    HttpClient.prototype.getCon = function (url, fn) {
        var _this = this;
        this.httpObj.open("GET", url, true);
        this.httpObj.onreadystatechange = function () {
            if (_this.httpObj.readyState == 4) {
                fn(_this.httpObj.responseText);
            }
        };
        this.httpObj.send(null);
    };
    return HttpClient;
}());
//# sourceMappingURL=app.js.map