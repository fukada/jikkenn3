//http://docs.nwjs.io/en/latest/References/Window/#windowgetwindow_object
let nw, win, window: Window, document: Document

export function main(nwjsModule) {

    nw = nwjsModule
    win = nw.Window.get()
    window = win.window
    document = window.document

    let url = "http://localhost:8080/"

    let can = new Canvas(document.getElementById("can"))

    let client = new HttpClient()

    let outputBox = document.getElementById("output") as HTMLTextAreaElement
    let outputButton = document.getElementById("outputbutton") as HTMLInputElement
    let resetButton = document.getElementById("resetbutton") as HTMLInputElement

    outputBox.innerHTML = ""

    outputButton.addEventListener("click", e => {
        client.postCon(url, can.charsData, true, data => {
            outputBox.innerHTML += `\nfrom server:(${data})\n`
        })
        outputBox.innerHTML = JSON.stringify(can.charsData)
    }, false)
    resetButton.addEventListener("click", e => {
        can.clearCanvas()
        outputBox.innerHTML = ""
    }, false)

    return
}

class Canvas {
    private can: HTMLCanvasElement
    private cc: CanvasRenderingContext2D
    public style: CSSStyleDeclaration

    private bx: number
    private by: number

    private chars: Moji

    line: number[][][]

    constructor(canvasElem: HTMLElement) {
        this.can = canvasElem as HTMLCanvasElement
        this.cc = this.can.getContext("2d")
        this.style = this.can.style

        this.can.width = 508
        this.can.height = 508

        this.style.border = "1px solid"
        this.style.touchAction = "none"

        this.can.addEventListener("mousedown", e => { this.getXYfromMouse(e, 0) }, false)
        this.can.addEventListener("mousemove", e => { this.getXYfromMouse(e, 1) }, false)
        this.can.addEventListener("mouseup", e => { this.getXYfromMouse(e, 2) }, false)
        this.can.addEventListener("mouseout", e => { this.getXYfromMouse(e, 2) }, false)

        this.can.addEventListener("touchstart", e => { this.getXYfromTouch(e, 0) }, false)
        this.can.addEventListener("touchmove", e => { this.getXYfromTouch(e, 1) }, false)
        this.can.addEventListener("touchend", e => { this.getXYfromTouch(e, 2) }, false)
        this.can.addEventListener("touchcancel", e => { this.getXYfromTouch(e, 2) }, false)

        this.chars = new Moji()
    }

    private mousedownf = false

    private getXYfromTouch(e: TouchEvent, n: number) {
        let cancli = this.can.getBoundingClientRect()
        let [x, y] = [
            e.changedTouches.item(0).pageX - cancli.left,
            e.changedTouches.item(0).pageY - cancli.top
        ]

        if (n == 0) {
            this.bx = x
            this.by = y
            this.chars.start()
        } else {
            this.draw(x, y)
        }

        this.chars.update(x, y)
    }

    private getXYfromMouse(e: MouseEvent, n: number) {
        let cancli = this.can.getBoundingClientRect()
        let [x, y] = [
            e.pageX - cancli.left,
            e.pageY - cancli.top
        ]

        if (n == 0) {
            this.mousedownf = true
            this.bx = x
            this.by = y
            this.chars.start()
            this.chars.update(x, y)
        } else if (this.mousedownf) {
            this.draw(x, y)
            if (n == 2 || this.isOutDisplay(x, y))
                this.mousedownf = false
            this.chars.update(x, y)
        }
    }

    draw(x, y) {
        this.cc.save()
        this.cc.beginPath()
        this.cc.strokeStyle = "rgb(0,0,0)"
        this.cc.lineWidth = 2
        this.cc.lineTo(this.bx, this.by)
        this.cc.lineTo(x, y)
        this.cc.stroke()
        this.cc.restore()

        this.bx = x
        this.by = y
    }

    get charsData() {
        return this.chars.line
    }

    isOutDisplay(x: number, y: number) {
        return (x < 0 || this.can.width < x || y < 0 || this.can.height < y) ? true : false
    }

    clearCanvas() {
        this.cc.clearRect(0, 0, this.can.width, this.can.height)
        this.chars.init()
    }

    outputPngData() {
        let image = this.can.toDataURL("image/png")
        return image
    }
}

class Moji {
    line: number[][]
    startf: 0 | 1

    constructor() {
        this.line = []
    }

    start() {
        this.startf = 0
    }

    update(x, y) {
        if (this.startf === undefined) return
        this.line.push([this.startf, x, y])
        if (!this.startf) this.startf = 1
    }

    init() {
        this.line = []
    }
}

class HttpClient {
    httpObj: XMLHttpRequest

    constructor() {
        this.httpObj = new XMLHttpRequest()
    }

    postImage(url: string, Base64Data: any, callback: (data: string) => any) {
        this.httpObj.open("POST", url, true)
        this.httpObj.setRequestHeader("Content-Type", "application/json")

        let blob = atob(Base64Data)

        this.httpObj.send(blob)

        this.httpObj.onreadystatechange = () => {
            if (this.httpObj.readyState == 4) {
                callback(this.httpObj.responseText)
            }
        }

        return undefined

    }

    postCon(url: string, postData: any, fasyn: boolean, callback: (data: string) => any = undefined) {
        this.httpObj.open("POST", url, fasyn)
        this.httpObj.setRequestHeader("Content-Type", "application/json")
        this.httpObj.send(JSON.stringify(postData))
        if (fasyn) {
            this.httpObj.onreadystatechange = () => {
                if (this.httpObj.readyState == 4) {
                    callback(this.httpObj.responseText)
                }
            }
            return undefined
        } else {
            callback = callback || ((data: any) => data)
            if (this.httpObj.readyState == 4) {
                return callback(this.httpObj.responseText)
            }
        }
    }

    public getCon(url: string, fn: (content: string) => void) {
        this.httpObj.open("GET", url, true)
        this.httpObj.onreadystatechange = () => {
            if (this.httpObj.readyState == 4) {
                fn(this.httpObj.responseText)
            }
        }
        this.httpObj.send(null)
    }


}