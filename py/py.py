import numpy as np

from keras.models import Sequential
from keras.layers import Dense

model = Sequential()

model.add(Dense(units=1, activation='relu', input_dim=100))
model.add(Dense(units=1, activation='sigmoid'))

model.compile(optimizer='adam',
              loss='binary_crossentropy',
              metrics=['accuracy'])

data_1_num = np.random.randint(0,high=100,size=(1000))
print (data_1_num)

data = np.zeros((1000, 100))

for i, data_num in enumerate(data_1_num):
    one_positions = np.random.choice(np.arange(0,100),  data_num, replace=False)
    data[i][one_positions] = 1




labels = np.array([i % 2 for i in data_1_num])

# 各イテレーションのバッチサイズを32で学習を行なう
model.fit(data, labels, epochs=1000, batch_size=200,validation_split=0.1, verbose=2)


#classes = model.predict(data,batch_size=128)
