import os
import re
import numpy as np

def get_data():
    data = []
    label = []

    for n in range(10):
    
        path = os.path.join("./","trainHWRT/" + str(n) + ".txt")

        with open(file=path,mode="r") as f:
            filedata = f.readlines()
            for s in filedata:
                if re.match('\[',s):
                    label.append(n)
                    data.append([])
                else:
                    data[len(label) - 1].extend(list(map(int,s.replace("\n","").split())))
    return data,label   
                        
