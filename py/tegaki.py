import numpy as np
import os

from read_file import get_data

from keras.models import Model,load_model
from keras.layers import Input,Dense,LSTM, Lambda,Masking
from keras import backend as K
from keras.optimizers import SGD
from keras.utils import plot_model,np_utils
from keras.preprocessing.sequence import pad_sequences


#モデルの定義
inputs = Input(shape=(None,3))
x = Masking(mask_value=0)(inputs)
x = LSTM(units=100,return_sequences=True)(x)
x = Lambda(lambda x: K.sum(x, axis=1))(x)
predictions = Dense(units=10, activation='softmax')(x)

model = Model(inputs=inputs,outputs=predictions)

model.compile(optimizer=SGD(lr=0.00001, momentum=0.9),
              loss='categorical_crossentropy',
              metrics=['accuracy'])


# データの作成
data_list,label_list = get_data()

#(sequence, time*3)

data_array = pad_sequences(data_list, dtype='float32', padding='post')
_data=np.reshape(data_array,(-1,3))

_data = ( _data - np.mean(_data, 0) ) / np.var(_data, 0)
print(_data[:10])


#data = (data - np.mean(data, axis=-1)) / np.var(data)
# pad_sequences

data = np.reshape(_data,(len(data_list), -1, 3))

labels = np.array(label_list)
#print(labels)
labels = np_utils.to_categorical(labels)

# 学習
model.fit(data, labels, epochs=100, batch_size=200,validation_split=0.1, verbose=2)



#保存と読み込み
model.save(os.path.join('./', 'lstm_model.h5'))

   
model.save_weights(os.path.join('./', 'lstm_model.hdf5'))



#データの予測
def predict(data):
    my_model = load_model(os.path.join('./', 'lstm_model.h5'))
    res = model.predict(data)
    return res


def output_model_image():
    plot_model(model,to_file='./model2.png')