
import numpy as np
import os

from read_file import get_data

from keras.models import Model,load_model
from keras.layers import Input,Dense,LSTM, Lambda,Masking
from keras import backend as K
from keras.optimizers import SGD
from keras.utils import plot_model,np_utils
from keras.preprocessing.sequence import pad_sequences
import random


#モデルの定義
inputs = Input(shape=(None,4))
x = Masking(mask_value=0)(inputs)
x = LSTM(units=100,return_sequences=True)(x)
x = LSTM(units=100,return_sequences=True)(x)
x = Lambda(lambda x: K.sum(x, axis=1))(x)
predictions = Dense(units=10, activation='softmax')(x)

model = Model(inputs=inputs,outputs=predictions)
model.summary()

model.compile(optimizer=SGD(lr=0.0001, momentum=0.9),
              loss='categorical_crossentropy',
              metrics=['accuracy'])


# データの作成
data_list,label_list = get_data()

data_all = list(zip(data_list, label_list))

random.shuffle(data_all)

data_list,label_list = zip(*data_all)

print (label_list)

#正規化
d_data = []
for i in range(len(data_list)):
    d_data.append([])
    for j in range(3,len(data_list[i]),3):
        dx = data_list[i][j + 1] - data_list[i][j - 3 + 1]
        dy = data_list[i][j + 2] - data_list[i][j - 3 + 2]
        dis = (dx ** 2 + dy ** 2) ** 0.5 + 0.001
        if data_list[i][j]== 0:
            f=1
        else:
            f=0

        d_data[i].append([dis,dx / dis,dy / dis,f])  

    d_temp = np.array(d_data[i])
    d_temp[:,0] = (d_temp[:,0]) / (np.mean(d_temp[:,0]) + 1)
    d_data[i] = d_temp
data_array = pad_sequences(d_data, dtype='float32', padding='post')
  

#(sequence, time*3)
_data = np.reshape(data_array,(-1,4))

# _data = (_data - np.mean(_data, 0)) / np.var(_data, 0)
print(_data[:100])


#data = (data - np.mean(data, axis=-1)) / np.var(data)
# pad_sequences
data = np.reshape(_data,(len(data_list), -1, 4))

labels = np.array(label_list)
#print(labels)
labels = np_utils.to_categorical(labels)

# 学習
model.fit(data, labels, epochs=100, batch_size=100,validation_split=0.1, verbose=2)



#保存と読み込み
model.save(os.path.join('./', 'lstm_model.h5'))

   
#model.save_weights(os.path.join('./', 'lstm_model.hdf5'))



#データの予測
def predict(data):
    my_model = load_model(os.path.join('./', 'lstm_model.h5'))
    res = mymodel.predict(data)
    return res


def output_model_image():
    plot_model(model,to_file='./model2.png')