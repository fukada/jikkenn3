from http_server import set_callback,importargs,run
import tegaki



def callback(req):
    print(req)
    return tegaki.predict(req)

def main():
    set_callback(callback)
    host, port = importargs()
    run(server_name=host, port=port)

if __name__ == '__main__':
    main()



